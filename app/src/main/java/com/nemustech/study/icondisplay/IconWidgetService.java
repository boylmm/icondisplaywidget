package com.nemustech.study.icondisplay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

public class IconWidgetService extends RemoteViewsService {
    private final int NUM_ICONS = 140;
    int[] mIconIds;
    @Override
    public void onCreate() {
        super.onCreate();

        mIconIds = new int[NUM_ICONS];
        String iconName;
        for (int idx = 0; idx < NUM_ICONS; ++idx) {
            iconName = String.format("test_icon_%03d", idx);
            try {
                mIconIds[idx] = R.drawable.class.getField(iconName).getInt(null);
            } catch (Throwable t) {
                // TODO Auto-generated catch block
                t.printStackTrace();
                mIconIds[idx] = R.drawable.icon;
            }
        }
    }

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent arg0) {
        // TODO Auto-generated method stub
        return new IconViewsFactory(getApplicationContext());
    }

    private void shuffleIcons() {
        if (null == mIconIds) {
            return;
        }
        int target;
        int temp;
        for (int idx = 0; idx < NUM_ICONS; ++idx) {
            target = (int)(Math.random() * NUM_ICONS);
            if (idx != target) {
                temp = mIconIds[target];
                mIconIds[target] = mIconIds[idx];
                mIconIds[idx] = temp;
            }
        }
    }

    class IconViewsFactory implements RemoteViewsService.RemoteViewsFactory {
        private Context mContext;

        IconViewsFactory(Context c) {
            mContext = c;
        }

        @Override
        public int getCount() {
            return mIconIds.length;
        }

        @Override
        public long getItemId(int position) {
            if (0 <= position && position < mIconIds.length) {
                return mIconIds[position];
            } else {
                return R.drawable.icon;
            }
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public RemoteViews getViewAt(int position) {
            // Get the data for this position from the content provider
            int resourceId = (int)getItemId(position);
            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.icons_item);
            rv.setImageViewResource(R.id.iv_icon, resourceId);

            // Set the click intent so that we can handle it and show a toast message
            final Intent fillInIntent = new Intent();
            final Bundle extras = new Bundle();
            extras.putInt(IconWidgetProvider.EXTRA_RESOURCE_ID, resourceId);
            fillInIntent.putExtras(extras);
            rv.setOnClickFillInIntent(R.id.iv_icon, fillInIntent);

            return rv;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public void onCreate() {
        }

        @Override
        public void onDataSetChanged() {
            shuffleIcons();
        }

        @Override
        public void onDestroy() {
        }
    }

}


