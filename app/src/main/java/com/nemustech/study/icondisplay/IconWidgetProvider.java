package com.nemustech.study.icondisplay;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.Toast;

public class IconWidgetProvider extends AppWidgetProvider {
    final static String ACTION_SHUFFLE = "com.nemustech.study.icondisplay.ACTION_SHUFFLE";
    final static String ACTION_CLICK = "com.nemustech.study.icondisplay.ACTION_CLICK";
    final static String EXTRA_RESOURCE_ID = "com.nemustech.study.icondisplay.RESOURCE_ID";

    private static HandlerThread sWorkerThread;
    private static Handler sWorkerQueue;

    public IconWidgetProvider() {
        sWorkerThread = new HandlerThread("WeatherWidgetProvider-worker");
        sWorkerThread.start();
        sWorkerQueue = new Handler(sWorkerThread.getLooper());
    }

    @Override
    public void onReceive(Context ctx, Intent intent) {
        final String action = intent.getAction();
        final Context c = ctx;
        if (ACTION_SHUFFLE.equals(action)) {
            sWorkerQueue.removeMessages(0);
            sWorkerQueue.post(new Runnable() {
                @Override
                public void run() {
                    final AppWidgetManager mgr = AppWidgetManager.getInstance(c);
                    final ComponentName cn = new ComponentName(c, IconWidgetProvider.class);
                    mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.grid_icons);
                }
            });
        } else if (ACTION_CLICK.equals(action)){
            int resId = intent.getIntExtra(EXTRA_RESOURCE_ID, 0);
            if (0 == resId) {
                Toast.makeText(c, R.string.invalid_image, Toast.LENGTH_SHORT);
            }
            ImageView iv = new ImageView(c);
            iv.setBackgroundColor(0xFF000000);
            iv.setPadding(5, 5, 5, 5);
            iv.setImageResource(resId);
            Toast toast = new Toast(c);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(iv);
            toast.show();
        }
        super.onReceive(ctx, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; ++i) {
            final Intent intent = new Intent(context, IconWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            final RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            rv.setRemoteAdapter(appWidgetIds[i], R.id.grid_icons, intent);
            
            final Intent onClickIntent = new Intent(context, IconWidgetProvider.class);
            onClickIntent.setAction(ACTION_CLICK);
            onClickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            final PendingIntent onClickPendingIntent = PendingIntent.getBroadcast(context, 0,
                    onClickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setPendingIntentTemplate(R.id.grid_icons, onClickPendingIntent);

            final Intent shuffleIntent = new Intent(context, IconWidgetProvider.class);
            shuffleIntent.setAction(ACTION_SHUFFLE);
            final PendingIntent refreshPendingIntent = PendingIntent.getBroadcast(context, 0,
                    shuffleIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setOnClickPendingIntent(R.id.tv_title, refreshPendingIntent);

            appWidgetManager.updateAppWidget(appWidgetIds[i], rv);
        }
    }
}
